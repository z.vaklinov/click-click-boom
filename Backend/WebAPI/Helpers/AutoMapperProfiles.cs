﻿using AutoMapper;
using WebAPI.Core.Models;
using WebAPI.DTOs;

namespace WebAPI.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Origin, OriginDto>().ReverseMap();
            CreateMap<Origin, UpdateOriginDto>().ReverseMap();
        }
    }
}
