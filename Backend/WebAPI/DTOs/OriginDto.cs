﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.DTOs
{
    public class OriginDto
    {
        public int Id { get; set; }
        [Required]
        public string? Name { get; set; }
        [Required]
        public string? Country { get; set; }

    }
}
