﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Core.Models;
using WebAPI.DTOs;

namespace WebAPI.Controllers
{
    [Authorize]
    public class OriginController : BaseController
    {
        private readonly IOriginRepository repository;
        private readonly IMapper mapper;

        public OriginController(IOriginRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetOrigins()
        {
            var origins = await repository.GetOriginsAsync();

            var originsDto = mapper.Map<IEnumerable<Origin>>(origins);

            return Ok(originsDto);
        }

        [HttpPost("post")]
        public async Task<IActionResult> AddOrigin(OriginDto request)
        {
            var origin = mapper.Map<Origin>(request);
            origin.LastUpdatedBy = 1;
            origin.LastUpdated = DateTime.UtcNow;

            repository.AddOrigin(origin);
            await repository.SaveAsync();
            return StatusCode(201);
        }

        [HttpPut("update/{id}")]
        public async Task<IActionResult> UpdateOrigin(int id, OriginDto request)
        {
            if (id != request.Id) return BadRequest();

            var originFromDb = await repository.GetOrigin(id);

            if (originFromDb is null) return BadRequest();

            originFromDb.LastUpdatedBy = 1;
            originFromDb.LastUpdated = DateTime.UtcNow;
            mapper.Map(request, originFromDb);
            await repository.SaveAsync();
            return StatusCode(200);
        }

        [HttpPut("updateOriginName/{id}")]
        public async Task<IActionResult> UpdateOrigin(int id, UpdateOriginDto request)
        {
            var originFromDb = await repository.GetOrigin(id);
            originFromDb.LastUpdatedBy = 1;
            originFromDb.LastUpdated = DateTime.UtcNow;
            mapper.Map(request, originFromDb);
            await repository.SaveAsync();
            return StatusCode(200);
        }

        [HttpPatch("update/{id}")]
        public async Task<IActionResult> UpdateOriginPatch(int id, JsonPatchDocument<Origin> OriginToPatch)
        {
            var originFromDb = await repository.GetOrigin(id);
            originFromDb.LastUpdatedBy = 1;
            originFromDb.LastUpdated = DateTime.UtcNow;

            OriginToPatch.ApplyTo(originFromDb, ModelState);
            await repository.SaveAsync();
            return StatusCode(200);
        }

        [HttpDelete("remove/{id}")]
        public async Task<IActionResult> RemoveOrigin(int id)
        {
            repository.RemoveOrigin(id);
            await repository.SaveAsync();
            return Ok(id);
        }
    }
}
