﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Core.Models
{
    public class Origin
    {
        public int Id { get; set; }
        [Required]
        public string? Name { get; set; }

        [Required]
        public string? Country { get; set; }
        
        public DateTime LastUpdated { get; set; }

        public int LastUpdatedBy { get; set; }
    }
}
