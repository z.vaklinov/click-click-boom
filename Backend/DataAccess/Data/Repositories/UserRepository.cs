﻿using Core.Models;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using WebAPI.DataAccess.Data;

namespace DataAccess.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext context;

        public UserRepository(DataContext context)
        {
            this.context = context;
        }
        public async Task<User> Authenticate(string userName, string password)
        {
            
            return await context.Users.FirstOrDefaultAsync(x => x.Username == userName 
            && x.Password == password);                     
        }
    }
}
