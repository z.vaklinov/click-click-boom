﻿using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using WebAPI.Core.Models;
using WebAPI.DataAccess.Data;

namespace DataAccess.Data.Repositories
{
    public class OriginRepository : IOriginRepository
    {
        private readonly DataContext context;
        public OriginRepository(DataContext context)
        {
            this.context = context;
        }
        public void AddOrigin(Origin origin)
        {
            context.Origins.AddAsync(origin);
        }

        public async Task<Origin> GetOrigin(int id)
        {
            return await context.Origins.FindAsync(id);
        }

        public async Task<IEnumerable<Origin>> GetOriginsAsync()
        {
            return await context.Origins.ToListAsync();
        }

        public void RemoveOrigin(int originId)
        {
            var origin = context.Origins.Find(originId);
            context.Origins.Remove(origin);
        }

        public async Task<bool> SaveAsync()
        {
            return await context.SaveChangesAsync() > 0;
        }
    }
}
