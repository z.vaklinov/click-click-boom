﻿using WebAPI.Core.Models;

namespace DataAccess.Interfaces
{
    public interface IOriginRepository
    {
        Task<IEnumerable<Origin>> GetOriginsAsync();

        void AddOrigin(Origin origin);

        void RemoveOrigin(int originId);

        Task<bool> SaveAsync();

        Task<Origin> GetOrigin(int id);
    }
}
