# Click Click Boom

This project was created as an assignment of the SoftUni Angular November 2022 course.

It's an application for creating and viewing offers for buying/selling firearms.

Parts of this project include:

- Creating the Angular App and ASP.NET Core WebAPI;

- Adding a Client side login and register function to our Angular application

- Adding routing to the Angular application;

- Using various types of binding;

- Building an UI using Bootstrap

- Angular Template forms and Reactive forms and validation

- Filtering function using pipes;

- Publishing the application to Firebase.



## Project status
This project is still not completed. It has been deployed to FireBase on: https://click-click-boom-app.web.app/


