import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';

import { AppComponent } from './app.component';
import { FirearmCatalogueComponent } from './firearm/firearm-catalogue/firearm-catalogue.component';
import { FirearmUnitComponent } from './firearm/firearm-unit/firearm-unit.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { ArmoryService } from './services/armory.service';
import { AddFirearmComponent } from './firearm/add-firearm/add-firearm.component';
import {FirearmSpecificationsComponent} from './firearm/firearm-specifications/firearm-specifications.component';
import { PageNotfoundComponent } from './errors/page-notfound/page-notfound.component';
import { UserRegisterComponent } from './user/user-register/user-register/user-register.component';
import { UserLoginComponent } from './user/user-login/user-login/user-login.component';
import { UserService } from './services/user.service';
import { AlertifyService } from './services/alertify.service';
import { AuthService } from './services/auth.service';
import { FirearmSpecificationsResolverService } from './firearm/firearm-specifications/firearm-specifications-resolver.service';
import { FilterPipe } from './pipes/filter.pipe';


const appRoutes: Routes = [
  {path: '', component: FirearmCatalogueComponent},
  {path: 'the-armory', component: FirearmCatalogueComponent},
  {path: 'buy-armament', component: FirearmCatalogueComponent},
  {path: 'sell-armament', component: FirearmCatalogueComponent},
  {path: 'add-armament', component: AddFirearmComponent},
  {path: 'firearm-specs/:id',
                              component: FirearmSpecificationsComponent,
                              resolve: {fra: FirearmSpecificationsResolverService}},
  {path: 'user/login', component: UserLoginComponent},
  {path: 'user/register', component: UserRegisterComponent},
  {path: '**', component: PageNotfoundComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    FirearmUnitComponent,
    FirearmCatalogueComponent,
    NavigationBarComponent,
    AddFirearmComponent,
    FirearmSpecificationsComponent,
    PageNotfoundComponent,
    UserRegisterComponent,
    UserLoginComponent,
    FilterPipe
   ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
    NgxGalleryModule
  ],
  providers: [
    ArmoryService,
    UserService,
    AlertifyService,
    AuthService,
    FirearmSpecificationsResolverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
