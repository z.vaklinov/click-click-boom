import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IFirearm } from 'src/app/model/IFirearm';
import { ArmoryService } from 'src/app/services/armory.service';


@Component({
  selector: 'app-firearm-catalogue',
  templateUrl: './firearm-catalogue.component.html',
  styleUrls: ['./firearm-catalogue.component.css']
})
export class FirearmCatalogueComponent implements OnInit {

  BuySell: number = 1;
  firearms: IFirearm[] = [];
  Origin = '';
  SearchOrigin = '';

  constructor(private route: ActivatedRoute, private armory: ArmoryService) { }

  ngOnInit(): void
  {
    if (this.route.snapshot.url.toString())
    {
      this.BuySell = 2;
    }

    this.armory.getAllFirearms(this.BuySell).subscribe(
      data=> {
        this.firearms = data;
        console.log(data);
      }
    )
  }


  onOriginFilter(){
    this.SearchOrigin = this.Origin;
  }

  onOriginFilterClear(){
    this.SearchOrigin = '';
    this.Origin = '';
  }



}
