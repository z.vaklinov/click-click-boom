import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { IFirearm } from '../../model/IFirearm';
import { Firearm } from 'src/app/model/firearm';
import { ArmoryService } from 'src/app/services/armory.service';
import { AlertifyService } from 'src/app/services/alertify.service';

@Component({
  selector: 'app-add-firearm',
  templateUrl: './add-firearm.component.html',
  styleUrls: ['./add-firearm.component.css']
})
export class AddFirearmComponent implements OnInit {
  //@ViewChild('Form') addFirearmForm!: NgForm;
  @ViewChild('formTabs') formTabs: TabsetComponent;

  addFirearmForm: FormGroup;
  nextClicked: boolean;
  firearm = new Firearm();

  firearmTypes: Array<string> = ['Semi-automatic pistol', 'Submachine gun', 'Assault rifle'];
  originList: any[];


  firearmView: IFirearm =
  {
    Id: 0,
    BuySell: null,
    Name: '',
    Origin: '',
    Action: null,
    Cartridge: null,
    EFR: null,
    Produced: null,
    Manufacturer: null,
    Type: null,
    Price: null
  };

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private armoryService: ArmoryService,
    private alertify: AlertifyService) { }

  ngOnInit()
  {
    this.CreateAddFirearmForm();
    this.armoryService.getAllOrigin().subscribe(data =>
      {
        this.originList = data;
        console.log(data);
      })
  }

  CreateAddFirearmForm() {
    this.addFirearmForm = this.fb.group({
        BasicInfo: this.fb.group({
            BuySell: ['1' , Validators.required],
            Type: [null, Validators.required],
            Name: [null, Validators.required],
            Price: [null, Validators.required],
        }),

        Specifications: this.fb.group({
            Origin: [null, Validators.required],
            Action: [null, Validators.required],
            Cartridge: [null, Validators.required],
            EFR: [null, Validators.required],
        }),

        ProductionHistory: this.fb.group({
            Produced: [null, Validators.required],
            Manufacturer: [null, Validators.required]
        })
    });
}

//#region FormGroups

get BasicInfo() {
  return this.addFirearmForm.controls['BasicInfo'] as FormGroup;
};

get Specifications() {
  return this.addFirearmForm.controls['Specifications'] as FormGroup;
};

get ProductionHistory() {
  return this.addFirearmForm.controls['ProductionHistory'] as FormGroup;
};


//#endregion

//#region FormControlls
get BuySell() {
  return this.BasicInfo.controls['BuySell'] as FormControl;
}
get Type() {
  return this.BasicInfo.controls['Type'] as FormControl;
}
get Name() {
  return this.BasicInfo.controls['Name'] as FormControl;
}
get Price() {
  return this.BasicInfo.controls['Price'] as FormControl;
}
get Origin() {
  return this.Specifications.controls['Origin'] as FormControl;
}
get Action() {
  return this.Specifications.controls['Action'] as FormControl;
}
get Cartridge() {
  return this.Specifications.controls['Cartridge'] as FormControl;
}
get EFR() {
  return this.Specifications.controls['EFR'] as FormControl;
}
get Produced() {
  return this.ProductionHistory.controls['Produced'] as FormControl;
}
get Manufacturer() {
  return this.ProductionHistory.controls['Manufacturer'] as FormControl;
}

//#endregion

  onBack()
  {
    this.router.navigate(['/']);
  }

  onSubmit(){
    this.nextClicked = true;
    if(this.allTabsValid())
    {
      this.mapFirearm();
      this.armoryService.addFirearm(this.firearm);
      this.alertify.success('Nice, that is a sweet offer, armbearer!');

      if(this.BuySell.value === '2')
      {
        this.router.navigate(['/sell-armament']);
      }
      else
      {
        this.router.navigate(['/']);
      }
    }
    else{
      this.alertify.error("Armbearer...please make all of your offer's inputs");
    }
  }

  mapFirearm() : void
  {
    this.firearm.Id = this.armoryService.newFirearmId();
    this.firearm.BuySell = +this.BuySell.value;
    this.firearm.Type = this.Type.value;
    this.firearm.Name = this.Name.value;
    this.firearm.Price = this.Price.value;
    this.firearm.Origin = this.Origin.value;
    this.firearm.Action = this.Action.value;
    this.firearm.Cartridge = this.Cartridge.value;
    this.firearm.EFR = this.EFR.value;
    this.firearm.Produced = this.Produced.value;
    this.firearm.Manufacturer = this.Manufacturer.value;
  }

  allTabsValid(): boolean {
    if(this.BasicInfo.invalid){
      this.formTabs.tabs[0].active = true;
      return false;
    }

    if(this.Specifications.invalid){
      this.formTabs.tabs[1].active = true;
      return false;
    }
    return true;
  }

  selectTab(NextTabId: number, IsCurrentTabValid: boolean)
  {
    this.nextClicked = true;
    if(IsCurrentTabValid)
    {
      this.formTabs.tabs[NextTabId].active = true;
    }
  }
}
