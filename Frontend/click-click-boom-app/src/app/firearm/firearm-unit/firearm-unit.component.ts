import { Component, Input } from "@angular/core";
import { IFirearm } from "src/app/model/IFirearm";



@Component({
  selector: 'app-firearm-unit',
  templateUrl: 'firearm-unit.component.html',
  styleUrls: ['firearm-unit.component.css']
}

)
export class FirearmUnitComponent{
  @Input() firearm: IFirearm;
  @Input() hideIcons: boolean;

}
