import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Firearm } from 'src/app/model/firearm';
import { ArmoryService } from 'src/app/services/armory.service';
import {NgxGalleryOptions} from '@kolkov/ngx-gallery';
import {NgxGalleryImage} from '@kolkov/ngx-gallery';
import {NgxGalleryAnimation} from '@kolkov/ngx-gallery';

@Component({
  selector: 'app-firearm-specifications',
  templateUrl: './firearm-specifications.component.html',
  styleUrls: ['./firearm-specifications.component.css']
})
export class FirearmSpecificationsComponent implements OnInit {

  public firearmId!: number;
  firearm = new Firearm();
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private armoryService: ArmoryService) { }

  ngOnInit() {
    this.firearmId = Number(this.route.snapshot.params['id']);
    this.route.data.subscribe(
      (data: Firearm) => {
        this.firearm = data['fra'];
      }
    );

    this.galleryOptions = [
      {
        width: '100%',
        height: '450px',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },

    ];

    this.galleryImages = [
      {
        small: 'assets/images/glock1.jpg',
        medium: 'assets/images/glock1.jpg',
        big: 'assets/images/glock1.jpg'
      },
      {
        small: 'assets/images/glock2.jpg',
        medium: 'assets/images/glock2.jpg',
        big: 'assets/images/glock2.jpg'
      },
      {
        small: 'assets/images/glock3.jpg',
        medium: 'assets/images/glock3.jpg',
        big: 'assets/images/glock3.jpg'
      },{
        small: 'assets/images/glock4.jpg',
        medium: 'assets/images/glock4.jpg',
        big: 'assets/images/glock4.jpg'
      },
      {
        small: 'assets/images/glock5.jpg',
        medium: 'assets/images/glock5.jpg',
        big: 'assets/images/glock5.jpg'
      }
    ];

    // this.route.params.subscribe(
    //   (params) =>
    //   {
    //     this.firearmId = Number(params['id']);
    //     this.armoryService.getFirearm(this.firearmId).subscribe(
    //       (data: Firearm) =>
    //       {
    //         this.firearm = data;
    //       }
    //     )
    //   }
    // );
  }



}
