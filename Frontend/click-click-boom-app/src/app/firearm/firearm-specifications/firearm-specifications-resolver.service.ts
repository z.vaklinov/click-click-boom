import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { catchError, Observable, of } from 'rxjs';
import { Firearm } from 'src/app/model/firearm';
import { ArmoryService } from 'src/app/services/armory.service';

@Injectable({
  providedIn: 'root'
})
export class FirearmSpecificationsResolverService implements Resolve<Firearm>{

constructor(private router: Router, private armoryService: ArmoryService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
   Observable<Firearm> | Firearm {
      const firearmId = route.params['id'];
      return this.armoryService.getFirearm(+firearmId).pipe(
        catchError(error => {
          this.router.navigate(['/']);
          return of(null);
        })
      );
   }

}
