/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FirearmSpecificationsResolverService } from './firearm-specifications-resolver.service';

describe('Service: FirearmSpecificationsResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirearmSpecificationsResolverService]
    });
  });

  it('should ...', inject([FirearmSpecificationsResolverService], (service: FirearmSpecificationsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
