/* tslint:disable:no-unused-variable */
import {ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FirearmSpecificationsComponent } from './firearm-specifications.component';

describe('FirearmSpecificationsComponent', () => {
  let component: FirearmSpecificationsComponent;
  let fixture: ComponentFixture<FirearmSpecificationsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FirearmSpecificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirearmSpecificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
