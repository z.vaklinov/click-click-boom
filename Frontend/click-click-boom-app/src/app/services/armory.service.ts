import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Firearm } from '../model/firearm';


@Injectable({
  providedIn: 'root'
})
export class ArmoryService {

constructor(private http: HttpClient) { }

  getAllOrigin(): Observable<string[]>
  {
    return this.http.get<string[]>('http://localhost:26822/api/origin');
  }

  getFirearm(id: number)
  {
    return this.getAllFirearms().pipe(map(firearmsArray =>
      {
        return firearmsArray.find(f => f.Id === id);
      }));
  }

  getAllFirearms(BuySell?: number): Observable<Firearm[]>

  {
    return this.http.get('data_context/firearms.json').pipe(
      map(data =>{

        let firearmsArray: Array<Firearm> = [];
        let localFirearms = JSON.parse(localStorage.getItem('newFirearm'));

        if(localFirearms)
        {
          for(const id in localFirearms)
          {
            if(BuySell)
            {

              if(localFirearms.hasOwnProperty(id) && localFirearms[id].BuySell === BuySell)
              {
               firearmsArray.push(localFirearms[id]);
              }
            }

            else
            {
            firearmsArray.push(localFirearms[id]);
            }
          }
        }

        for(const id in data)
        {
          if(BuySell)
          {

            if(data.hasOwnProperty(id) && data[id].BuySell === BuySell)
            {
             firearmsArray.push(data[id]);
            }
          }
          else
          {
            firearmsArray.push(data[id]);
          }
        }
        return firearmsArray;
      })
    );

    return this.http.get<Firearm[]>('data_context/firearms.json');
  }

  addFirearm(firearm: Firearm)
  {
    let newFirearm = [firearm];

    // Storing the firearms in an array on local storage so each previously created firearm doesn't get overriden by a new one.
    if(localStorage.getItem('newFirearm'))
    {
      newFirearm = [Object.keys(firearm), ...JSON.parse(localStorage.getItem('newFirearm'))];
    }

    localStorage.setItem('newFirearm', JSON.stringify(newFirearm));
  }

  newFirearmId()
  {
    if(localStorage.getItem('FID'))
    {
      localStorage.setItem('FID', String(+localStorage.getItem('FID') + 1));
      return +localStorage.getItem('FID');
    }
    else{
      localStorage.setItem('FID','101');
      return 101;
    }
  }
}
