import { Component, OnInit } from '@angular/core';
import { AlertifyService } from '../services/alertify.service';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {

  loggedInUser: string;
  constructor(private alertify: AlertifyService) { }

  ngOnInit() {
  }

  loggedIn()
  {
    this.loggedInUser = localStorage.getItem('token');
    return this.loggedInUser;
  }

  onLogout()
  {
    localStorage.removeItem('token');
    this.alertify.success("See you soon, armbearer.");
  }

}
