export interface IFirearm {
  Id: number;
  BuySell: number;
  Name: string;
  Origin: string;
  Action: string;
  Cartridge: string;
  EFR: string;
  Produced: string;
  Manufacturer: string;
  Type: string;
  Price: string;
  Image?: string;
}
