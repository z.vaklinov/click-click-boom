import { IFirearm } from "./IFirearm";


export class Firearm implements IFirearm {
  Id: number;
  BuySell: number;
  Name: string;
  Origin: string;
  Action: string;
  Cartridge: string;
  EFR: string;
  Produced: string;
  Manufacturer: string;
  Type: string;
  Price: string;
  Image?: string;
  ExtraDescription?: string;

}
