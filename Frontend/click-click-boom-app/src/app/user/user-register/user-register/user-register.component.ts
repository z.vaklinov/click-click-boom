import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/services/user.service';
import { AlertifyService } from 'src/app/services/alertify.service';


@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css'],
})
export class UserRegisterComponent implements OnInit {

  registrationForm!: FormGroup
  user: User;
  userSubmitted: boolean;

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private alertifyService: AlertifyService) { }

  ngOnInit()
  {
    // this.registrationForm = new FormGroup(
    //   {
    //     userName: new FormControl(null, [Validators.required, Validators.minLength(9)]),
    //     email: new FormControl(null, [Validators.required, Validators.email, Validators.minLength(9)]),
    //     password: new FormControl(null, [Validators.required, Validators.minLength(8)]),
    //     confirmPassword: new FormControl(null, [Validators.required]),
    //     possessionCertificateNumber: new FormControl(null, [Validators.required, Validators.minLength(12), Validators.maxLength(12)])
    //   }, this.passwordMatchValidator);

    this.createRegistrationForm();
  }

  createRegistrationForm()
  {
    this.registrationForm = this.fb.group({
      userName: [null, Validators.required],
      email: [ null, [Validators.required, Validators.email, Validators.minLength(9)]],
      password: [null, [Validators.required, Validators.minLength(8)]],
      confirmPassword: [null, Validators.required],
      possessionCertificateNumber: [null, Validators.required]
    }, {validators: this.passwordMatchValidator});
  }

  passwordMatchValidator(fc: AbstractControl): ValidationErrors | null
  {
    return fc.get('password')?.value === fc.get('confirmPassword')?.value ? null :
      { notmatched: true }
  };

  onSubmit()
  {
    this.userSubmitted = true;

    if(this.registrationForm.valid){
      this.userService.addUser(this.userData());
      this.registrationForm.reset();
      this.userSubmitted = false;
      this.alertifyService.success("You have registered successfully. Welcome, armbearer!");
    }
    else
    {
      this.alertifyService.error("Kindly provide valid credentials, armbearer!");
    }
  }

  userData(): User
  {
    return this.user =
    {
      userName: this.userName.value,
      email: this.email.value,
      password: this.password.value,
      confirmPassword: this.confirmPassword.value,
      possessionCertificateNumber: this.possessionCertificateNumber.value
    };
  }

  get userName(){
    return this.registrationForm.get('userName') as FormControl;
  }

  get email(){
    return this.registrationForm.get('email') as FormControl;
  }

  get password(){
    return this.registrationForm.get('password') as FormControl;
  }

  get confirmPassword(){
    return this.registrationForm.get('confirmPassword') as FormControl;
  }

  get possessionCertificateNumber(){
    return this.registrationForm.get('possessionCertificateNumber') as FormControl;
  }

}
